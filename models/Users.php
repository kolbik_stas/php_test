<?php

namespace app\models;
use yii\web\IdentityInterface;
use Yii;


class Users extends \yii\db\ActiveRecord 
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password_hash'], 'required'],
            [['username', 'email', 'password_hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password_hash' => 'Password',
        ];
    }
    public function validatePassword($password)
    {
        return $hash = Yii::$app->getSecurity()->generatePasswordHash($password, $this->password_hash);
    }
 
}
